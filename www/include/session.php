<?php
session_start();
if (! isset($_SESSION["idUtilisateur"])) {
    header("Location: /connexion.php");
    exit();
}
?>
