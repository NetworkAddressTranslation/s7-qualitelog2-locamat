<?php
include("modele/utilisateur.php");

$mail = $_POST["email"];
$mdp = $_POST["mdp"];

function verifierCompte($mail, $mdp) {
    $utilisateur = Utilisateur::connexion($mail, $mdp);
    if ($utilisateur) {
        session_start();
        $_SESSION["nom"] = $utilisateur->get_nom();
        $_SESSION["prenom"] = $utilisateur->get_prenom();
        $_SESSION["idUtilisateur"] = $utilisateur->get_id();
        header("Location: index.php");
    } else {
        header("Location: connexion.php?erreur=1&mail=" . $mail);
    }
}

verifierCompte($mail, $mdp);

?>
