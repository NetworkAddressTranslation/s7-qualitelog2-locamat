<!DOCTYPE html>
<html lang="fr" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>Connexion</title>
    </head>
    <body>
        <h1>Connexion</h1>
        <form method="POST" action="login.php">
            <?php if (isset($_GET["erreur"]) && $_GET["erreur"] == "1") {?>
                <label class="erreur">L'adresse mail ou le mot de passe est incorrect.</label>
            <?php } ?>
            <div>
                <label for="email">Adresse email :</label>
                <input type="email" name="email" id="email" placeholder="email"
                    <?php if (isset($_GET["mail"])) { echo "value=\"" . $_GET["mail"] . "\""; } ?>
                    required>
            </div>
            <div>
                <label for="mdp">Mot de passe :</label>
                <input type="password" name="mdp" id="mdp" placeholder="mot de passe" required>
            </div>
            <div>
                <button type="submit" name="connexion">Connexion</button>
            </div>
        </form>
    </body>
</html>
