<?php

include_once(__DIR__ . "/../bdd.php");

class Materiel {
    private $id;
    private $nom;
    private $version;
    private $ref;
    private $photo;
    private $telephone;
    private $emprunteur;
    private $debut;
    private $fin;

    public static function construct_donnees($id, $nom, $version, $ref, $photo, $telephone, $emprunteur, $debut, $fin) {
        $instance = new self();
        $instance->id         = $id;
        $instance->nom        = $nom;
        $instance->version    = $version;
        $instance->ref        = $ref;
        $instance->photo      = $photo;
        $instance->telephone  = $telephone;
        $instance->emprunteur = $emprunteur;
        $instance->debut      = $debut;
        $instance->fin        = $fin;
        return $instance;
    }

    public function get_id() {
        return $this->id;
    }

    public function get_nom() {
        return $this->nom;
    }

    public function get_version() {
        return $this->version;
    }

    public function get_ref() {
        return $this->ref;
    }

    public function get_photo() {
        return $this->photo;
    }

    public function get_telephone() {
        return $this->telephone;
    }

    public function get_emprunteur() {
        return $this->emprunteur;
    }

    public function get_debut() {
        return $this->debut;
    }

    public function get_fin() {
        return $this->fin;
    }
}

?>
