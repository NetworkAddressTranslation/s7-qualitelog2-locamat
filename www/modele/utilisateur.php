<?php

include_once(__DIR__ . "/../bdd.php");

Class Utilisateur {
    private $id;
    private $nom;
    private $prenom;
    private $mail;
    private $mdp;
    private $matricule;
    private $role;

    public static function construct_donnees($id, $nom, $prenom, $mail, $mdp, $matricule, $role) {
        $instance = new self();
        $instance->id        = $id;
        $instance->nom       = $nom;
        $instance->prenom    = $prenom;
        $instance->mail      = $mail;
        $instance->mdp       = $mdp;
        $instance->matricule = $matricule;
        $instance->role      = $role;
        return $instance;
    }

    public static function connexion($mail, $mdp) {
        $utilisateur = utilisateur_connect($mail, $mdp);
        return $utilisateur ? $utilisateur : false;
    }

    public function get_id() {
        return $this->id;
    }

    public function get_nom() {
        return $this->nom;
    }

    public function get_prenom() {
        return $this->prenom;
    }

    public function get_mail() {
        return $this->mail;
    }

    public function get_mdp() {
        return $this->mdp;
    }

    public function get_matricule() {
        return $this->matricule;
    }

    public function get_role() {
        return $this->role;
    }

}
?>
