<?php
require_once(__DIR__ . "/include/session.php");
include(__DIR__ . "/bdd.php");
?>

<!DOCTYPE html>
<html lang="fr" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>Accueil</title>
    </head>

    <body>
        <?php require_once(__DIR__ . "/include/header.php"); ?>
        <h1>Accueil</h1>
        <h2>Liste du matériel informatique</h2>
        <table>
            <thead>
                <tr>
                    <td>Nom</td>
                    <td>Consultation</td>
                </tr>
            </thead>
            <?php
                $liste_materiel = get_liste_materiel();

                foreach ($liste_materiel as $materiel) {
                    ?>
                    <tr>
                        <td>
                            <?php echo $materiel->get_nom(); ?>
                        </td>
                        <td>
                            <a href=<?php echo "\"/materiel/index.php?id=" . $materiel->get_id() . "\""; ?>>
                                Lien
                            </a>
                        </td>
                    </tr>
                    <?php
                }
            ?>
        </table>

        <!-- Seulement pour les admins : -->
        <?php
        if (utilisateur_est_admin($_SESSION["idUtilisateur"]))
        {
        ?>
            <a href="/materiel/form_creer.php">Créer un nouveau matériel</a>
            <h2>Liste des utilisateurs</h2>
            <table>
                <thead>
                    <tr>
                        <td>Nom</td>
                        <td>Consultation</td>
                    </tr>
                </thead>
                <?php
                    $liste_utilisateurs = get_liste_utilisateur();

                    foreach ($liste_utilisateurs as $utilisateur) {
                        ?>
                        <tr>
                            <td>
                                <?php echo $utilisateur->get_prenom() . " " . $utilisateur->get_nom(); ?>
                            </td>
                            <td>
                                <a href=<?php echo "\"/utilisateur/index.php?id=" . $utilisateur->get_id() . "\""; ?>>
                                    Lien
                                </a>
                            </td>
                        </tr>
                        <?php
                    }
                ?>
            </table>
            <a href="/utilisateur/form_creer.php">Créer un nouvel utilisateur</a>
        <?php
        }
        ?>
    </body>
</html>
