<?php
session_start();

if (!array_key_exists("nom", $_SESSION)) {
    header("Location: /connexion.php");
} else {
	include_once(__DIR__ . "/../bdd.php");

	if (!utilisateur_est_admin($_SESSION["idUtilisateur"])) {
		include_once(__DIR__ . "/../erreur/403.php");
	} else {
		$nom = $_POST["nom"];
		$prenom = $_POST["prenom"];
		$email = $_POST["email"];
		$mdp = $_POST["mdp"];
		$matricule = $_POST["matricule"];
		$role = $_POST["role"];

		$id = creer_utilisateur($nom, $prenom, $email, $mdp, $matricule, $role);

		if ($id) {
			header("Location: index.php?id=" . $id);
		} else {
			header("Location: form_creer.php?erreur=1" .
			"&nom=" . $nom .
			"&prenom=" . $prenom .
			"&email=" . $email .
			"&mdp=" . $mdp .
			"&matricule=" . $matricule
			);
		}
	}
}
?>
