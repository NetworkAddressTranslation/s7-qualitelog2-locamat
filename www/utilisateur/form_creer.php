<?php
require_once(__DIR__ . "/../include/session.php");
include_once(__DIR__ . "/../bdd.php");

if (!utilisateur_est_admin($_SESSION["idUtilisateur"])) {
    include_once(__DIR__ . "/../erreur/403.php");
} else {
?>

<!DOCTYPE html>
<html lang="fr" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>Création d’un nouveau utilisateur</title>
    </head>

    <body>
        <?php require_once(__DIR__ . "/../include/header.php"); ?>
        <h1>Création d’un nouveau utilisateur</h1>
        <form action="/utilisateur/creer.php" method="POST">
            <?php if (isset($_GET["erreur"]) && $_GET["erreur"] == "1") {?>
                <label class="erreur">Il y a eu une erreur lors de la création de l'utilisateur.</label>
            <?php } ?>
            <div>
                <label for="nom">Nom*</label>
                <input type="text" name="nom" minlength=1 maxlength=30
                <?php if (isset($_GET["nom"])) { echo "value=\"" . $_GET["nom"] . "\""; } ?>
                required />
            </div>
            <div>
                <label for="prenom">Prénom*</label>
                <input type="text" name="prenom" minlength=1 maxlength=30
                <?php if (isset($_GET["prenom"])) { echo "value=\"" . $_GET["prenom"] . "\""; } ?>
                required />
            </div>
            <div>
                <label for="email">Adresse email*</label>
                <input type="email" name="email" id="email" pattern="^[^@\s]+@[^@\s]+\.[^@\s]+$"
                title="Doit être au format x@x.x"
                    <?php if (isset($_GET["mail"])) { echo "value=\"" . $_GET["mail"] . "\""; } ?>
                    required>
            </div>
			<div>
                <label for="mdp">Mot de passe*</label>
                <input type="password" name="mdp" id="mdp"
                    <?php if (isset($_GET["mdp"])) { echo "value=\"" . $_GET["mdp"] . "\""; } ?>
                    required>
            </div>
            <div>
                <label for="matricule">Matricule*</label>
                <input type="text" name="matricule" minlength=7 maxlength=7
                <?php if (isset($_GET["matricule"])) { echo "value=\"" . $_GET["matricule"] . "\""; } ?>
                required />
            </div>
			Rôle*
			<select name="role">
				<option>Emprunteur</option>
				<option>Administrateur</option>
			</select>
			<br>
            <input type="submit" value="Envoyer" />
        </form>
        * Champ obligatoire
    </body>
</html>

<?php } ?>
