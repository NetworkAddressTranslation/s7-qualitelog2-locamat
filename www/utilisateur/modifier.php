<?php
session_start();

include_once(__DIR__ . "/../bdd.php");

if (!array_key_exists("nom", $_SESSION)) {
    header("Location: /connexion.php");
} else if (!utilisateur_est_admin($_SESSION["idUtilisateur"])) {
	include_once(__DIR__ . "/../erreur/403.php");
} else {
	$id = $_POST["id"];
	$nom = $_POST["nom"];
	$prenom = $_POST["prenom"];
	$email = $_POST["email"];
	$mdp = $_POST["mdp"];
	$matricule = $_POST["matricule"];
	$role = $_POST["role"];

	$error = modifier_utilisateur($id, $nom, $prenom, $email, $mdp, $matricule, $role);

	if (!$error) {
		header("Location: index.php?id=" . $id);
	} else {
		header("Location: form_modifier.php?erreur=1" .
		"&nom=" . $nom .
		"&prenom=" . $prenom .
		"&email=" . $email .
		"&mdp=" . $mdp .
		"&matricule=" . $matricule
		);
	}
}
?>
