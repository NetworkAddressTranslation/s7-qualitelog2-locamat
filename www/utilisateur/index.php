<?php
require_once(__DIR__ . "/../include/session.php");
include(__DIR__ . "/../bdd.php");

if (!utilisateur_est_admin($_SESSION["idUtilisateur"])) {
    include_once(__DIR__ . "/../erreur/403.php");
} else {
	$id = $_GET['id'];
	$utilisateur = consulter_utilisateur($id);
?>

<html lang="fr" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title><?php echo $utilisateur->get_prenom()." ".$utilisateur->get_nom(); ?></title>
    </head>

    <body>
        <?php require_once(__DIR__ . "/../include/header.php"); ?>
        <h1>Consultation d'un utilisateur</h1>
        <table>
                    <tr>
                        <td>
                            <?php echo "Nom: "; ?>
                        </td>
                        <td>
                            <?php echo $utilisateur->get_nom(); ?>
                        </td>
                    </tr>
					<tr>
                        <td>
                            <?php echo "Prénom: "; ?>
                        </td>
						<td>
                            <?php echo $utilisateur->get_prenom(); ?>
                        </td>
                    </tr>
					<tr>
                        <td>
                            <?php echo "Adresse email: "; ?>
                        </td>
                        <td>
                            <?php echo $utilisateur->get_mail(); ?>
                        </td>
                    </tr>
					<tr>
                        <td>
                            <?php echo "Matricule: "; ?>
                        </td>
                        <td>
                            <?php echo $utilisateur->get_matricule(); ?>
                        </td>
                    </tr>
					<tr>
                        <td>
                            <?php echo "Rôle: "; ?>
                        </td>
                        <td>
                            <?php echo $utilisateur->get_role(); ?>
                        </td>
                    </tr>
        </table>
		<form action="/utilisateur/supprimer.php" method="POST">
			<input type="hidden" name="id" value="<?=$utilisateur->get_id()?>" />
			<input type="submit" value="Supprimer" />
		</form>

		<a href=<?php echo "\"/utilisateur/form_modifier.php?id=" . $utilisateur->get_id() . "\""; ?>>
		<br>Modifier
		</a>
    </body>
</html>

<?php
}
?>
