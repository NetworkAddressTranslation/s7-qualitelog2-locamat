<?php
session_start();

include_once(__DIR__ . "/../bdd.php");

if (!array_key_exists("nom", $_SESSION)) {
    header("Location: /connexion.php");
} else if (!utilisateur_est_admin($_SESSION["idUtilisateur"])) {
	include_once(__DIR__ . "/../erreur/403.php");
} else {
	$id = $_POST["id"];

	$error = supprimer_utilisateur($id);

	if (!$error) {
		header("Location: /index.php");
	} else {
		header("Location: index.php?id=".$id."&erreur=1");
	}
}
?>
