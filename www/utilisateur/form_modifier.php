<?php
require_once(__DIR__ . "/../include/session.php");
include_once(__DIR__ . "/../bdd.php");

if (!utilisateur_est_admin($_SESSION["idUtilisateur"])) {
    include_once(__DIR__ . "/../erreur/403.php");
} else {
	$id = $_GET['id'];
	$utilisateur = consulter_utilisateur($id);
	$nom = isset($_GET['nom']) ? $_GET['nom'] : $utilisateur->get_nom();
	$prenom = isset($_GET['prenom']) ? $_GET['nom'] : $utilisateur->get_prenom();
	$mail = isset($_GET['mail']) ? $_GET['mail'] : $utilisateur->get_mail();
	$mdp = isset($_GET['mdp']) ? $_GET['mdp'] : $utilisateur->get_mdp();
	$matricule = isset($_GET['matricule']) ? $_GET['matricule'] : $utilisateur->get_matricule();
    $role = isset($_GET['role']) ? $_GET['role'] : $utilisateur->get_role();
?>

<!DOCTYPE html>
<html lang="fr" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>Modification d’un utilisateur</title>
    </head>

    <body>
        <?php require_once(__DIR__ . "/../include/header.php"); ?>
        <h1>Modification d’un utilisateur</h1>
        <form action="/utilisateur/modifier.php". method="POST">
            <?php if (isset($_GET["erreur"]) && $_GET["erreur"] == "1") {?>
                <label class="erreur">Il y a eu une erreur lors de la modification de l'utilisateur.</label>
            <?php } ?>
			<div>
                <label for="id"></label>
                <input type="hidden" name="id" value=<?php echo $utilisateur->get_id(); ?>>
            </div>
            <div>
                <label for="nom">Nom*</label>
				<input type="text" name="nom" minlength=1 maxlength=30 value="<?=$nom?>" required />
            </div>
            <div>
                <label for="prenom">Prénom*</label>
				<input type="text" name="prenom" minlength=1 maxlength=30 value="<?=$prenom?>" required />
            </div>
            <div>
                <label for="email">Adresse email*</label>
				<input type="email" name="email" id="email" pattern="^[^@\s]+@[^@\s]+\.[^@\s]+$"
                title="Doit être au format x@x.x" value="<?=$mail?>" required>
            </div>
			<div>
                <label for="mdp">Mot de passe*</label>
				<input type="password" name="mdp" id="mdp" value="<?=$mdp?>" required>
            </div>
            <div>
                <label for="matricule">Matricule*</label>
				<input type="text" name="matricule" minlength=7 maxlength=7 value="<?=$matricule?>" required />
            </div>
			Rôle*
            <select name="role">
				<option <?php if ($role == "Emprunteur") echo "selected"; ?>>Emprunteur</option>
				<option <?php if ($role == "Administrateur") echo "selected"; ?>>Administrateur</option>
			</select>
			<br>
            <input type="submit" value="Envoyer" />
        </form>
        * Champ obligatoire
    </body>
</html>
<?php } ?>
