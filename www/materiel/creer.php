<?php
session_start();

include_once(__DIR__ . "/../bdd.php");

if (!array_key_exists("nom", $_SESSION)) {
    header("Location: /connexion.php");
} else if (!utilisateur_est_admin($_SESSION["idUtilisateur"])) {
	include_once(__DIR__ . "/../erreur/403.php");
} else {
	$nom = $_POST["nom"];
	$version = $_POST["version"];
	$reference = $_POST["ref"];
	$photo = $_POST["photo"];
	$telephone = $_POST["tel"];

	$id = ajouter_materiel($nom, $version, $reference, $photo, $telephone);

	if ($id) {
		header("Location: index.php?id=" . $id);
	} else {
		header("Location: form_creer.php?erreur=1" .
		"&nom=" . $nom .
		"&version=" . $version .
		"&ref=" . $reference .
		"&tel=" . $telephone
		);
	}
}
?>
