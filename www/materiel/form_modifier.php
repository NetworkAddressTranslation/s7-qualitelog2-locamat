<?php
require_once(__DIR__ . "/../include/session.php");
include_once(__DIR__ . "/../bdd.php");

if (!utilisateur_est_admin($_SESSION["idUtilisateur"])) {
    include_once(__DIR__ . "/../erreur/403.php");
} else {
	$id = $_GET['id'];
	$materiel = consulter_materiel($id);
?>

<!DOCTYPE html>
<html lang="fr" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>Modification d’un matériel</title>
    </head>

    <body>
        <?php require_once(__DIR__ . "/../include/header.php"); ?>
        <h1>Modification d’un matériel</h1>
        <form action="/materiel/modifier.php" method="POST">
            <?php if (isset($_GET["erreur"]) && $_GET["erreur"] == "1") {?>
                <label class="erreur">Il y a eu une erreur lors de la modification du matériel.</label>
            <?php } ?>
			<input type="hidden" name="id" value="<?=$materiel->get_id()?>">
            <div>
                <label for="nom">Nom*</label>
                <input type="text" name="nom" minlength=1 maxlength=30 value="<?=$materiel->get_nom()?>"
                required />
            </div>
            <div>
                <label for="version">Version*</label>
                <input type="text" name="version" minlength=3 maxlength=15 value="<?=$materiel->get_version()?>"
                required />
            </div>
            <div>
                <label for="ref">Ref*</label>
                <input type="text" name="ref" minlength=5 maxlength=5 pattern="(AN|AP|XX)[0-9]{3}" value="<?=$materiel->get_ref()?>"
                title="Doit commencer par AN pour Android, AP pour Apple, XX sinon et se termine par 3 chiffres."
                required />
            </div>
            <div>
                <label for="photo">Photo</label>
                <input type="file" name="photo" accept=".jpg,.jpeg" />
            </div>
            <div>
                <label for="tel">Numéro de téléphone</label>
                <input type="tel" name="tel" minlength=10 maxlength=10 value="<?=$materiel->get_telephone()?>" />
            </div>
            <input type="submit" value="Envoyer" />
        </form>
        * Champ obligatoire
    </body>
</html>

<?php } ?>
