<?php
require_once(__DIR__ . "/../include/session.php");
include_once(__DIR__ . "/../bdd.php");

if (!utilisateur_est_admin($_SESSION["idUtilisateur"])) {
    include_once(__DIR__ . "/../erreur/403.php");
} else {
?>

<!DOCTYPE html>
<html lang="fr" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>Création d’un nouveau matériel informatique</title>
    </head>

    <body>
        <?php require_once(__DIR__ . "/../include/header.php"); ?>
        <h1>Création d’un nouveau matériel informatique</h1>
        <form action="/materiel/creer.php" method="POST">
            <?php if (isset($_GET["erreur"]) && $_GET["erreur"] == "1") {?>
                <label class="erreur">Il y a eu une erreur lors de la création du matériel.</label>
            <?php } ?>
            <div>
                <label for="nom">Nom*</label>
                <input type="text" name="nom" minlength=1 maxlength=30
                <?php if (isset($_GET["nom"])) { echo "value=\"" . $_GET["nom"] . "\""; } ?>
                required />
            </div>
            <div>
                <label for="version">Version*</label>
                <input type="text" name="version" minlength=3 maxlength=15
                <?php if (isset($_GET["version"])) { echo "value=\"" . $_GET["version"] . "\""; } ?>
                required />
            </div>
            <div>
                <label for="ref">Ref*</label>
                <input type="text" name="ref" minlength=5 maxlength=5 pattern="(AN|AP|XX)[0-9]{3}"
                title="Doit commencer par AN pour Android, AP pour Apple, XX sinon et se termine par 3 chiffres."
                <?php if (isset($_GET["ref"])) { echo "value=\"" . $_GET["ref"] . "\""; } ?>
                required />
            </div>
            <div>
                <label for="photo">Photo</label>
                <input type="file" name="photo" accept=".jpg,.jpeg" />
            </div>
            <div>
                <label for="tel">Numéro de téléphone</label>
                <input type="tel" name="tel" minlength=10 maxlength=10
                <?php if (isset($_GET["tel"])) { echo "value=\"" . $_GET["tel"] . "\""; } ?>
                />
            </div>
            <input type="submit" value="Envoyer" />
        </form>
        * Champ obligatoire
    </body>
</html>

<?php } ?>
