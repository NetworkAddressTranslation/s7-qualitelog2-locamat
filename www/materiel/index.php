<?php
require_once(__DIR__ . "/../include/session.php");
include_once(__DIR__ . "/../bdd.php");

$id = $_GET['id'];
$materiel = consulter_materiel($id);
?>

<html lang="fr" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title><?php echo $materiel->get_nom(); ?></title>
    </head>

    <body>
        <?php require_once(__DIR__ . "/../include/header.php"); ?>
        <h1>Consultation d'un matériel</h1>
        <table>
                    <tr>
                        <td>
                            <?php echo "Nom: "; ?>
                        </td>
                        <td>
                            <?php echo $materiel->get_nom(); ?>
                        </td>
                    </tr>
					<tr>
                        <td>
                            <?php echo "Photo: "; ?>
                        </td>
                        <td>
                            <?php
								if($materiel->get_photo() == NULL)
									echo '';
							?>
                        </td>
                    </tr>
					<tr>
                        <td>
                            <?php echo "Version: "; ?>
                        </td>
                        <td>
                            <?php echo $materiel->get_version(); ?>
                        </td>
                    </tr>
					<tr>
                        <td>
                            <?php echo "Ref: "; ?>
                        </td>
                        <td>
                            <?php echo $materiel->get_ref(); ?>
                        </td>
                    </tr>
					<tr>
                        <td>
                            <?php echo "Téléphone: "; ?>
                        </td>
                        <td>
                            <?php echo $materiel->get_telephone(); ?>
                        </td>
                    </tr>
					<tr>
                        <td>
                            <?php echo "Emprunteur: "; ?>
                        </td>
                        <td>
                            <?php echo $materiel->get_emprunteur(); ?>
                        </td>
                    </tr>
					<tr>
                        <td>
                            <?php echo "Date de début d'emprunt: "; ?>
                        </td>
                        <td>
                            <?php echo $materiel->get_debut(); ?>
                        </td>
                    </tr>
					<tr>
                        <td>
                            <?php echo "Date de fin d'emprunt: "; ?>
                        </td>
                        <td>
                            <?php echo $materiel->get_fin(); ?>
                        </td>
                    </tr>
        </table>
		<!-- Seulement pour les admins : -->
        <?php
        if (utilisateur_est_admin($_SESSION["idUtilisateur"]))
        {
        ?>
			<form action="/materiel/supprimer.php" method="POST">
				<input type="hidden" name="id" value="<?=$materiel->get_id()?>" />
				<input type="submit" value="Supprimer" />
			</form>

            <a href=<?php echo "\"/materiel/form_modifier.php?id=" . $materiel->get_id() . "\""; ?>>
            <br>Modifier
			</a>
        <?php
        }
        ?>
    </body>
</html>
