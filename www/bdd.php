<?php

include_once("modele/utilisateur.php");
include_once("modele/materiel.php");

if (defined('PHPUNIT') && PHPUNIT) {
	define('CONFIG_PATH', "./config.txt");
} else {
	define('CONFIG_PATH', $_SERVER['DOCUMENT_ROOT'] . "/../config.txt");
}

function read_config($name)
{
	$file = file($name);

	if ($file == false) {
		return null;
	}

	return array(
		"address" => rtrim($file[0]),
		"user" => rtrim($file[1]),
		"password" => rtrim($file[2]),
		"database" => rtrim($file[3]),
		"port" => intval(rtrim($file[4]))
	);
}

define('CONFIG', read_config(CONFIG_PATH));

function bdd_connect()
{
    $mydb = new mysqli(CONFIG["address"], CONFIG["user"], CONFIG["password"], CONFIG["database"], CONFIG["port"]);
	if ($mydb->connect_errno) {
		echo "Failed to connect to MySQL: (" . $mydb->connect_errno . ") " . $mydb->connect_error;
	}
	return $mydb;
}

function utilisateur_mapper($resultat_bdd) {
	if ($resultat_bdd != null) {
		$utilisateur = Utilisateur::construct_donnees(
			$resultat_bdd["IDUTILISATEUR"],
			$resultat_bdd["NOMUTILISATEUR"],
			$resultat_bdd["PRENOM"],
			$resultat_bdd["MAIL"],
			$resultat_bdd["MDP"],
			$resultat_bdd["MATRICULE"],
			$resultat_bdd["ROLE"]
		);
		return $utilisateur;
	}
	return null;
}

function liste_utilisateurs_mapper($resultats_bdd) {
    $liste_utilisateurs = array();
    foreach ($resultats_bdd as $utilisateur) {
        array_push($liste_utilisateurs, utilisateur_mapper($utilisateur));
    }
    return $liste_utilisateurs;
}

function materiel_mapper($resultat_bdd) {
	if ($resultat_bdd != null) {
		$materiel = Materiel::construct_donnees(
			$resultat_bdd["IDMATERIEL"],
			$resultat_bdd["NOMMATERIEL"],
			$resultat_bdd["VERSION"],
			$resultat_bdd["REF"],
			$resultat_bdd["PHOTO"],
			$resultat_bdd["TELEPHONE"],
			$resultat_bdd["EMPRUNTEUR"],
			$resultat_bdd["DEBUT"],
			$resultat_bdd["FIN"]
		);
		return $materiel;
	}
	return null;
}

function liste_materiel_mapper($resultats_bdd) {
    $liste_materiel = array();
    foreach ($resultats_bdd as $materiel) {
        array_push($liste_materiel, materiel_mapper($materiel));
    }
    return $liste_materiel;
}

/*
 * Fonction de connexion au site web.
 *
 * @param str $mail L'adresse email de l'utilisateur.
 * @param str $mdp Le mot de passe de l'utilisateur.
 * @return Utilisateur L'utilisateur connecté si l'email et le mot de passe donnés correspondent à un utilisateur,
 *         ou null sinon.
 */
function utilisateur_connect($mail, $mdp)
{
    $db = bdd_connect();
	$sql="select * from UTILISATEUR";
	$result = $db->query($sql);
	while($data = $result->fetch_assoc()){
		if($data["MAIL"] == $mail && $data["MDP"] == $mdp) {
			return utilisateur_mapper($data);
		}
	}
	return null;
}

/*
 * Fonction de listage du matériel.
 *
 * @return array(Materiel) La liste du matériel dans la base de données.
 */
function get_liste_materiel()
{
	$db = bdd_connect();
	$sql="select * from MATERIEL";
	$result = $db->query($sql);
	return liste_materiel_mapper($result->fetch_all(MYSQLI_ASSOC));
}

/*
 * Fonction de listage des utilisateurs.
 *
 * @return array(Materiel) La liste des utilisateurs dans la base de données.
 */
function get_liste_utilisateur()
{
	$db = bdd_connect();
	$sql="select * from UTILISATEUR";
	$result = $db->query($sql);
	return liste_utilisateurs_mapper($result->fetch_all(MYSQLI_ASSOC));
}

/*
 * @param str $idUtilisateur L'identifiant d'un utilisateur.
 * @return str Le rôle de l'utilisateur, ou false si cet utilisateur n'existe pas.
 */
function get_role_utilisateur_selon_id($idUtilisateur)
{
    $db = bdd_connect();
	$sql="select * from UTILISATEUR";
	$result = $db->query($sql);
	while($data = $result->fetch_assoc()){
		if($data["IDUTILISATEUR"] == $idUtilisateur) {
			return $data["ROLE"];
		}
	}
	return false;
}

/*
 * @param str $idUtilisateur L'identifiant d'un utilisateur.
 * @return bool Si l'utilisateur est un administrateur.
 */
function utilisateur_est_admin($idUtilisateur)
{
    $role = get_role_utilisateur_selon_id($idUtilisateur);
    return $role ? $role == "Administrateur" : false;
}

function ajouter_materiel($nom, $version, $reference, $photo, $telephone)
{
    $error = false;
    $db = bdd_connect();
	$sql="insert into MATERIEL(NOMMATERIEL, VERSION, REF, PHOTO, TELEPHONE) values (?, ?, ?, ?, ?)";
	if (!($statement = $db->prepare($sql))) {
        $error = true;
        echo "Failed to prepare statement: (" . $db->errno . ") " . $db->error;
    }

    if (!$error && !$statement->bind_param("sssbs", $nom, $version, $reference, $photo, $telephone)) {
        $error = true;
        echo "Failed to bind parameters: (" . $statement->errno . ") " . $statement->error;
    }

    if (!$error && !$statement->execute()) {
        echo "Failed to execute: (" . $statement->errno . ") " . $statement->error;
    }

    return $error ? null : $db->insert_id;
}

/*
 * Fonction permettant de modifier un matériel dans la base de données.
 *
 * @param int $id L'identifiant du matériel à modifier.
 * @param str $version La nouvelle version du matériel.
 * @param str $ref La nouvelle référence du matériel.
 * @param str $photo La nouvelle photo du matériel.
 * @param str $tel Le nouveau numéro de téléphone de la personne prêtant le matériel. 10 caractères numériques.
 * @param int $emprunteur L'identifiant de l'utilisateur empruntant le matériel.
 * @param str $debut La date de début de l'emprunt du matériel, au format AAAA-MM-JJ.
 * @param str $fin La date de fin de l'emprunt du matériel, au format AAAA-MM-JJ.
 * @return bool Si la modification a échouée.
 */
function modifier_materiel($id, $nom, $version, $ref, $photo, $tel, $emprunteur, $debut, $fin)
{
	$error = false;
	$db = bdd_connect();
	$sql = "UPDATE MATERIEL SET NOMMATERIEL = ?,VERSION = ?,REF = ?,PHOTO = ?,TELEPHONE = ?,EMPRUNTEUR = ?,DEBUT = ?,FIN = ? WHERE IDMATERIEL = ?";
	if (!($stmt = $db->prepare($sql))) {
        $error = true;
        echo "Failed to prepare statement: (" . $db->errno . ") " . $db->error;
    }

	if (!$error && !$stmt->bind_param('sssbsissi', $nom, $version, $ref, $photo, $tel, $emprunteur, $debut, $fin, $id)) 	{
        $error = true;
        echo "Failed to bind parameters: (" . $stmt->errno . ") " . $stmt->error;
    }

	if (!$error && !$stmt->execute()) {
        echo "Failed to execute: (" . $stmt->errno . ") " . $stmt->error;
    }

	$stmt->close();
	$db ->close();
	return $error;
}

function creer_utilisateur($nom, $prenom, $mail, $mdp, $matricule, $role)
{
    $error = false;
	$db = bdd_connect();
	$sql = "INSERT INTO UTILISATEUR(NOMUTILISATEUR, PRENOM, MAIL, MDP, MATRICULE, ROLE) VALUES (?, ?, ?, ?, ?, ?)";
	if (!($stmt = $db->prepare($sql))) {
        $error = true;
        echo "Failed to prepare statement: (" . $db->errno . ") " . $db->error;
    }

	if (!$error && !$stmt->bind_param('ssssss', $nom, $prenom, $mail, $mdp, $matricule, $role))
	{
        $error = true;
        echo "Failed to bind parameters: (" . $stmt->errno . ") " . $stmt->error;
    }

	if (!$error && !$stmt->execute()) {
        $error = true;
        echo "Failed to execute: (" . $stmt->errno . ") " . $stmt->error;
    }

    $idUtilisateur = $db->insert_id;
	$stmt->close();
	$db ->close();
    return $error ? null : $idUtilisateur;
}

function modifier_utilisateur($id, $nom, $prenom, $mail, $mdp, $matricule, $role)
{
    $error = false;
	$db = bdd_connect();
	$sql = "UPDATE UTILISATEUR SET NOMUTILISATEUR = ?,PRENOM = ?,MAIL = ?,MDP = ?,MATRICULE = ?,ROLE = ? WHERE IDUTILISATEUR = ?";
	if (!($stmt = $db->prepare($sql))) {
        $error = true;
        echo "Failed to prepare statement: (" . $db->errno . ") " . $db->error;
    }

	if (!$error && !$stmt->bind_param('ssssssi', $nom, $prenom, $mail, $mdp, $matricule, $role, $id))
	{
        $error = true;
        echo "Failed to bind parameters: (" . $stmt->errno . ") " . $stmt->error;
    }

	if (!$error && !$stmt->execute()) {
        echo "Failed to execute: (" . $stmt->errno . ") " . $stmt->error;
    }

	$stmt->close();
	$db ->close();
	return $error;
}

function supprimer_utilisateur($id)
{
    $error = false;
	$db = bdd_connect();
	$sql = "DELETE FROM UTILISATEUR WHERE IDUTILISATEUR = ?";
	if (!($stmt = $db->prepare($sql))) {
        $error = true;
        echo "Failed to prepare statement: (" . $db->errno . ") " . $db->error;
    }

	if (!$error && !$stmt->bind_param('i', $id))
	{
        $error = true;
        echo "Failed to bind parameters: (" . $stmt->errno . ") " . $stmt->error;
    }

	if (!$error && !$stmt->execute()) {
        echo "Failed to execute: (" . $stmt->errno . ") " . $stmt->error;
    }

	$stmt->close();
	$db ->close();
}

function consulter_materiel($id)
{
	$error = false;
	$db = bdd_connect();
	$sql = "select * from MATERIEL where idmateriel = ?";
	if (!($stmt = $db->prepare($sql))) {
        $error = true;
        echo "Failed to prepare statement: (" . $db->errno . ") " . $db->error;
    }

	if (!$error && !$stmt->bind_param('i', $id))
	{
        $error = true;
        echo "Failed to bind parameters: (" . $stmt->errno . ") " . $stmt->error;
    }

	if (!$error && !$stmt->execute()) {
        echo "Failed to execute: (" . $stmt->errno . ") " . $stmt->error;
    }

	return $error ? null : materiel_mapper($stmt->get_result()->fetch_assoc());
}

function consulter_utilisateur($id)
{
    $error = false;
	$db = bdd_connect();
	$sql = "select * from UTILISATEUR where IDUTILISATEUR = ?";
	if (!($stmt = $db->prepare($sql))) {
        $error = true;
        echo "Failed to prepare statement: (" . $db->errno . ") " . $db->error;
    }

	if (!$error && !$stmt->bind_param('i', $id))
	{
        $error = true;
        echo "Failed to bind parameters: (" . $stmt->errno . ") " . $stmt->error;
    }

	if (!$error && !$stmt->execute()) {
        echo "Failed to execute: (" . $stmt->errno . ") " . $stmt->error;
    }

	return $error ? false : utilisateur_mapper($stmt->get_result()->fetch_assoc());
}

/*
 * Fonction permettant de réserver un matériel dans la base de données.
 *
 * @param int $id L'identifiant du matériel à réserver.
 * @param int $emprunteur L'identifiant de l'utilisateur empruntant le matériel.
 * @param str $debut La date de début de l'emprunt du matériel, au format AAAA-MM-JJ.
 * @param str $fin La date de fin de l'emprunt du matériel, au format AAAA-MM-JJ.
 * @return bool Si la modification a échouée.
 */
function reserver_materiel($id, $emprunteur, $debut, $fin)
{
	$error = false;
	$db = bdd_connect();
	$sql = "UPDATE MATERIEL SET EMPRUNTEUR = ?,DEBUT = ?,FIN = ? WHERE IDMATERIEL = ?";
	if (!($stmt = $db->prepare($sql))) {
        $error = true;
        echo "Failed to prepare statement: (" . $db->errno . ") " . $db->error;
    }

	if (!$error && !$stmt->bind_param('issi', $emprunteur, $debut, $fin, $id))
	{
        $error = true;
        echo "Failed to bind parameters: (" . $stmt->errno . ") " . $stmt->error;
    }

	if (!$error && !$stmt->execute()) {
        echo "Failed to execute: (" . $stmt->errno . ") " . $stmt->error;
    }

	$stmt->close();
	$db ->close();
}

function supprimer_materiel($id)
{
    $error = false;
	$db = bdd_connect();
	$sql = "DELETE FROM MATERIEL WHERE IDMATERIEL = ?";
	if (!($stmt = $db->prepare($sql))) {
        $error = true;
        echo "Failed to prepare statement: (" . $db->errno . ") " . $db->error;
    }

	if (!$error && !$stmt->bind_param('i', $id))
	{
        $error = true;
        echo "Failed to bind parameters: (" . $stmt->errno . ") " . $stmt->error;
    }

	if (!$error && !$stmt->execute()) {
        echo "Failed to execute: (" . $stmt->errno . ") " . $stmt->error;
    }

	$stmt->close();
	$db ->close();
}
?>
