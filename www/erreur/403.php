<?php http_response_code(403); ?>

<!DOCTYPE html>
<html lang="fr" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>Erreur 403</title>
    </head>
    <body>
        <h1>Erreur 403</h1>
        <p>Vous n'avez pas les droits nécessaires pour accéder à cette page.</p>        
    </body>
</html>
