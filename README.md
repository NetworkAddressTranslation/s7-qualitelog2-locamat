# Locamat

## Installation

Pour héberger ce site web, il faut un serveur PHP et MySQL.

Pour PHP, il faut régler le serveur pour que le répertoire racine soit "www".

Pour MySQL, il faut créer une nouvelle base de données et lancer le script "sql/creation.sql" dans celle-ci. Pour la suppression, il faut utiliser le script "sql/suppression.sql".

Il faut renseigner les paramètres de connexion de la base de données dans le fichier "config.txt", sous le format suivant :

```
Adresse du serveur MySQL
Utilisateur ayant accès en lecture et écriture à la base de données du site web
Mot de passe de cette utilisateur
Nom de la base de données du site web
Port du serveur MySQL
```

De base, il existe deux utilisateurs dans la base de données : un administrateur ayant pour adresse email "jean.michel@mail.fr" et pour mot de passe "azerty", et une emprunteuse ayant pour adresse email "marcelle.dupont@mail.fr" et pour mot de passe "qsdfg".

## Tests

Il est nécessaire pour les tests que le site web soit opérationnel comme décris ci-dessus. Il faut aussi recréer la base de données avec "sql/creation.sql" avant et après les tests, pour s'assurer que les données de base sont correctes.

Il faut ensuite installer les dépendances avec composer avec la commande `composer install`, et lancer les tests avec la commande `./vendor/bin/phpunit tests`.
