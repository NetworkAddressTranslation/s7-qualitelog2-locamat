source suppression.sql;

CREATE TABLE IF NOT EXISTS UTILISATEUR (
    IDUTILISATEUR INTEGER NOT NULL UNIQUE AUTO_INCREMENT,
    NOMUTILISATEUR VARCHAR(30) NOT NULL,
    PRENOM VARCHAR(30) NOT NULL,
    MAIL VARCHAR(50) NOT NULL UNIQUE,
    MDP VARCHAR(50) NOT NULL,
    MATRICULE VARCHAR(7) NOT NULL UNIQUE,
    ROLE SET('Administrateur','Emprunteur') NOT NULL DEFAULT 'Emprunteur',
    PRIMARY KEY(IDUTILISATEUR)
);

CREATE TABLE IF NOT EXISTS MATERIEL (
    IDMATERIEL INTEGER NOT NULL UNIQUE AUTO_INCREMENT,
    NOMMATERIEL VARCHAR(30) NOT NULL,
    VERSION VARCHAR(15) NOT NULL,
    REF VARCHAR(5) NOT NULL UNIQUE,
    PHOTO BLOB NULL,
    TELEPHONE VARCHAR(10) NULL,
    EMPRUNTEUR INTEGER NULL,
    DEBUT DATE NULL,
    FIN DATE NULL,
    PRIMARY KEY(IDMATERIEL),
    FOREIGN KEY(EMPRUNTEUR) REFERENCES UTILISATEUR(IDUTILISATEUR)
);

INSERT INTO UTILISATEUR (NOMUTILISATEUR, PRENOM, MAIL, MDP, MATRICULE, ROLE) VALUES
	("Jean", "Michel", "jean.michel@mail.fr", "azerty", "AJ248FS", "Administrateur"),
	("Marcelle", "Dupont", "marcelle.dupont@mail.fr", "qsdfg", "RD329RY", "Emprunteur");
