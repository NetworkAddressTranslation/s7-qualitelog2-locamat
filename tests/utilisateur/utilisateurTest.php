<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;
require_once __DIR__ . "/../../www/modele/utilisateur.php";
require_once __DIR__ . "/../../www/bdd.php";

final class UtilisateurTest extends TestCase
{
    private static $id;
    private static $nom       = "Nom";
    private static $prenom    = "Prenom";
    private static $mail      = "nom.prenom.test@mail.ext";
    private static $mdp       = "Kf85!fs@7";
    private static $matricule = "0000000";
    private static $role      = "Emprunteur";

    public function testAddNewUser(): void
    {
        static::$id = creer_utilisateur(
            static::$nom,
            static::$prenom,
            static::$mail,
            static::$mdp,
            static::$matricule,
            static::$role
        );
        $this->assertNotNull(static::$id);

        $utilisateur_cree  = consulter_utilisateur(static::$id);
        $utilisateur_voulu = Utilisateur::construct_donnees(
            static::$id,
            static::$nom,
            static::$prenom,
            static::$mail,
            static::$mdp,
            static::$matricule,
            static::$role
        );

        $this->assertEquals($utilisateur_cree, $utilisateur_voulu);
    }

    /**
     * @depends testAddNewUser
     */
    public function testAddNewUserWithExistingMailFail(): void
    {
        $id = creer_utilisateur(
            static::$nom,
            static::$prenom,
            static::$mail,
            static::$mdp,
            "",
            static::$role
        );
        $this->assertNull($id);
    }

    /**
     * @depends testAddNewUser
     */
    public function testAddNewUserWithExistingMatriculeFail(): void
    {
        $id = creer_utilisateur(
            static::$nom,
            static::$prenom,
            static::$mail . "2",
            static::$mdp,
            static::$matricule,
            static::$role
        );
        $this->assertNull($id);
    }

    /**
     * @depends testAddNewUser
     */
    public function testUpdateUser(): void
    {
        static::$nom       = static::$nom . "2";
        static::$prenom    = static::$prenom . "2";
        static::$mail      = static::$mail . "2";
        static::$mdp       = static::$mdp . "2";
        static::$matricule = "ZZZZZZZ";
        static::$role      = static::$role == "Emprunteur" ? "Administrateur" : "Emprunteur";

        modifier_utilisateur(
            static::$id,
            static::$nom,
            static::$prenom,
            static::$mail,
            static::$mdp,
            static::$matricule,
            static::$role
        );

        $utilisateur_cree  = consulter_utilisateur(static::$id);
        $utilisateur_voulu = Utilisateur::construct_donnees(
            static::$id,
            static::$nom,
            static::$prenom,
            static::$mail,
            static::$mdp,
            static::$matricule,
            static::$role
        );

        $this->assertEquals($utilisateur_cree, $utilisateur_voulu);
    }

    /**
     * @depends testUpdateUser
     */
    public function testUserInDatabaseCanConnect(): void
    {
        $utilisateur_connecte = utilisateur_connect(static::$mail, static::$mdp);

        $this->assertNotNull($utilisateur_connecte);
    }

    public function testUserNotInDatabaseCannotConnect(): void
    {
        $utilisateur_connecte = utilisateur_connect("inexistant", "inexistant");

        $this->assertNull($utilisateur_connecte);
    }

    /**
     * @depends testUpdateUser
     */
    public function testUserIsAdmin(): void
    {
        $utilisateur_connecte = utilisateur_connect(static::$mail, static::$mdp);

        $this->assertNotNull($utilisateur_connecte);
        $this->assertEquals($utilisateur_connecte->get_role(), "Administrateur");
    }

    /**
     * @depends testUserInDatabaseCanConnect
     * @depends testUserIsAdmin
     */
    public function testDeleteUser(): void
    {
        supprimer_utilisateur(static::$id);
        $utilisateur = consulter_utilisateur(static::$id);
        $this->assertNull($utilisateur);
    }
}
