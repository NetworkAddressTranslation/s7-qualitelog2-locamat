<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;
require_once __DIR__ . "/../../www/modele/materiel.php";
require_once __DIR__ . "/../../www/bdd.php";

final class MaterialTest extends TestCase
{
    private static $id;
    private static $nom     = "Samsung Galaxy S7";
    private static $version = "V8.0";
    private static $ref     = "AN345";
    private static $idEmprunteur;

    public function testAddNewMaterial(): void
    {
        static::$id = ajouter_materiel(
            static::$nom,
            static::$version,
            static::$ref,
			null, // photo
			null  // telephone
        );
        $this->assertNotNull(static::$id);

        $materiel_ajoute = consulter_materiel(static::$id);
		$materiel_voulu = Materiel::construct_donnees(
			static::$id,
			static::$nom,
			static::$version,
			static::$ref,
			null,
			null,
			null,
			null,
			null
		);

        $this->assertEquals($materiel_ajoute, $materiel_voulu);
    }

	/**
	 * @depends testRentMaterial
	 */
    public function testUpdateMaterial(): void
    {
		modifier_materiel(
			static::$id,
			static::$nom . "2",
			static::$version . ".2",
			"AN678",
			null,
			null,
			null,
			null,
			null
		);

        $materiel_modifie = consulter_materiel(static::$id);
		$materiel_voulu = Materiel::construct_donnees(
			static::$id,
			static::$nom . "2",
			static::$version . ".2",
			"AN678",
			null,
			null,
			null,
			null,
			null
		);

		$this->assertEquals($materiel_modifie, $materiel_voulu);
    }

	/**
	 * @depends testAddNewMaterial
	 */
    public function testRentMaterial(): void
    {
        static::$idEmprunteur = creer_utilisateur(
            "Nom",
            "Prenom",
            "nom.prenom.test@mail.ext",
            "Kf85!fs@7",
            "0000000",
            "Emprunteur"
        );
        $this->assertNotNull(static::$idEmprunteur);

		reserver_materiel(
			static::$id,
			static::$idEmprunteur,
			"2020-11-23",
			"2021-01-09"
		);

        $materiel_modifie = consulter_materiel(static::$id);
		$materiel_voulu = Materiel::construct_donnees(
			static::$id,
			static::$nom,
			static::$version,
			static::$ref,
			null,
			null,
			static::$idEmprunteur,
			"2020-11-23",
			"2021-01-09"
		);

		$this->assertEquals($materiel_modifie, $materiel_voulu);
    }

	/**
	 * @depends testUpdateMaterial
     * @depends testRentMaterial
	 */
    public function testDeleteMaterial(): void
    {
        supprimer_materiel(static::$id);
        $materiel = consulter_materiel(static::$id);
        $this->assertNull($materiel);

        supprimer_utilisateur(static::$idEmprunteur);
        $emprunteur = consulter_utilisateur(static::$idEmprunteur);
        $this->assertNull($emprunteur);
    }
}
